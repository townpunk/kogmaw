import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Review extends Component {

  render() {
    console.log(this.props.orderForm.meal)
    return (
      <div>
      <table>
        <tbody>
        <tr>
          <td>Meal</td>
          <td>{this.props.orderForm.meal}</td>
        </tr>
        <tr>
          <td>Number of People</td>
          <td>{this.props.orderForm.numberOfPeople}</td>
        </tr>
        <tr>
          <td>Restaurant</td>
          <td>{this.props.orderForm.restaurant}</td>
        </tr>
        <tr>
          <td>Dishes</td>
          <td>
            {this.props.orderForm.dishes.map((object, i) =>
              <ul key={i}>
                <li>
                  <span>{object.dish}</span>
                  <span>{object.numberOfServings}</span>
                </li>
              </ul>
            )}
          </td>
        </tr>
        </tbody>
      </table>
      <button onClick={this.props.previousStep}>Edit</button>
      <button onClick={this.handleSubmit}>Submit</button>
      </div>
    );
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.props.handleSubmit()
  }
}

export default Review
